#include <stdio.h>
#include <stdlib.h>

#define MAX 0x100

void pon_titulo(int num){ /* Parámetro formal */
		  char titulo[MAX];
			sprintf(titulo, "toilet -fpagga --metal tabla del %i", num);
			system(titulo);
}

int main(){
 		/* Declaración de variables */
		int num;
		int i;
		int res;
	 	
		/* MENU */
		printf(" Dime la tabla que quieres: \n");
		scanf(" %i", &num);

		/* Titulo */
		pon_titulo(num); /* llamada con parametro */
		
		/* Resultados */
		for(i=1; i<11; i++){
		res = num * i;
			printf(" %i*%i=%i \n", i, num, res);
			}
		 return EXIT_SUCCESS;
}

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(){
	/*printf tiene escritura bufferizada*/
	//printf(stderr, "hola\n");
	//puts("hola", stderr);
	fprintf(stderr, "\a"); /*stderr no esta bufferizado */
	usleep(10000); 
	fputc('\a', stderr);
	usleep(10000);
	printf("\a\n");
	
   return EXIT_SUCCESS;
}
